# Change the default tmux prefix
set -g prefix C-a

# Use C-a a to send the prefix through to applications running inside tmux
bind a send-prefix

# Smart pane switching with awareness of Neovim splits
bind-key -n C-h if -F "#{@pane-is-vim}" 'send-keys C-h' 'select-pane -L'
bind-key -n C-j if -F "#{@pane-is-vim}" 'send-keys C-j' 'select-pane -D'
bind-key -n C-k if -F "#{@pane-is-vim}" 'send-keys C-k' 'select-pane -U'
bind-key -n C-l if -F "#{@pane-is-vim}" 'send-keys C-l' 'select-pane -R'

# Move to last/previous pane
bind-key -n C-\\ if -F "#{@pane-is-vim}" 'send-keys C-\\' 'select-pane -l'

# Move around in copy mode
bind-key -T copy-mode-vi 'C-h' select-pane -L
bind-key -T copy-mode-vi 'C-j' select-pane -D
bind-key -T copy-mode-vi 'C-k' select-pane -U
bind-key -T copy-mode-vi 'C-l' select-pane -R
bind-key -T copy-mode-vi 'C-\' select-pane -l

# Set up the status bar
if-shell "test -f ~/.theme.tmux" "source ~/.theme.tmux"

# Allow mouse usage for pane resizing, copy-mode activation, etc.
set -g mouse on

# Enable italics, undercurls and true colors by selecting the appropriate
# terminfo and forcing tmux to assume the outer terminal supports true color.
# See https://ryantravitz.com/blog/2023-02-18-pull-of-the-undercurl/ for
# details
set -g default-terminal "tmux-256color"
set -gas terminal-overrides "*:Tc"
set -gas terminal-overrides "*:RGB"
set -as terminal-overrides ',*:Smulx=\E[4::%p1%dm'
set -as terminal-overrides ',*:Setulc=\E[58::2::%p1%{65536}%/%d::%p1%{256}%/%{255}%&%d::%p1%{255}%&%d%;m'

# Initialize plugins installed through TPM
set -g @plugin 'tmux-plugins/tpm'
# Add some conveniences for searching through files, git status, IP addresses,
# etc.
set -g @plugin 'tmux-plugins/tmux-copycat'
# 'Continuously' (i.e. every 15 minutes) save the state of Tmux using
# `tmux-resurrect` and restore the state when Tmux is restarted
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @continuum-restore 'on'
# Enable easily opening files/URLs, etc. by selecting them and pressing `o` and
# to edit selections using `C-o`
set -g @plugin 'tmux-plugins/tmux-open'
# Enable additional controls for managing panes (creating, navigating,
# resizing, etc.)
set -g @plugin 'tmux-plugins/tmux-pain-control'
# Enable 'resurrecting' Tmux sessions (e.g. after restarts). Also restores
# (Neo)vim sessions if a session file is available. These can easily be managed
# using vim-obsession.
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @resurrect-capture-pane-contents 'on'
set -g @resurrect-strategy-nvim 'session'
set -g @resurrect-strategy-vim 'session'
# Provides sensible defaults for using Tmux relative to the default values.
# This includes some settings relevant for integration with (Neo)vim (e.g.
# lowering `escape-time`)
set -g @plugin 'tmux-plugins/tmux-sensible'
# Make it easier to manage Tmux sessions from _within_ a Tmux session. This
# combines well with `tmux-resurrect` by preventing the need to create a new
# Tmux session upon startup and instead having some other session being
# restored
set -g @plugin 'tmux-plugins/tmux-sessionist'

run '~/.tmux/plugins/tpm/tpm'

# Map `C-k` to clear the scrollback buffer and `C-l` to clear the visible
# scrollback lines (both after the prefix is pressed).

# This needs to be configured _after_ the `tmux-pain-control` plugin is loaded
# as it defines the same bindings for window navigation ('pane down' and 'pane
# right' respectively)
bind C-k clear-history
bind C-l send-keys "C-l"
