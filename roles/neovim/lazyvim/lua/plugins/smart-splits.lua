return {
  "mrjones2014/smart-splits.nvim",
  keys = {
    {
      "<C-h>",
      function()
        require("smart-splits").move_cursor_left()
      end,
      desc = "Move to window to the left",
    },
    {
      "<C-l>",
      function()
        require("smart-splits").move_cursor_right()
      end,
      desc = "Move to window to the right",
    },
    {
      "<C-j>",
      function()
        require("smart-splits").move_cursor_down()
      end,
      desc = "Move to window below",
    },
    {
      "<C-k>",
      function()
        require("smart-splits").move_cursor_up()
      end,
      desc = "Move to window above",
    },
    {
      "<C-\\>",
      function()
        require("smart-splits").move_cursor_previous()
      end,
      desc = "Move to previous window",
    },
    {
      "<leader><C-h>",
      function()
        require("smart-splits").swap_buf_left()
      end,
      desc = "Swap with buffer to the left",
    },
    {
      "<leader><C-l>",
      function()
        require("smart-splits").swap_buf_right()
      end,
      desc = "Swap with buffer to the right",
    },
    {
      "<leader><C-j>",
      function()
        require("smart-splits").swap_buf_down()
      end,
      desc = "Swap with buffer below",
    },
    {
      "<leader><C-k>",
      function()
        require("smart-splits").swap_buf_up()
      end,
      desc = "Swap with buffer above",
    },
  },
  lazy = false,
}
