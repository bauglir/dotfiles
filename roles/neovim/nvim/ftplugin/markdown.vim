nnoremap <buffer> <Leader>P <Cmd>MarkdownPreviewToggle<CR>

let g:mkdp_preview_options = {
\   'uml': {
\     'server': 'https://kroki.io/plantuml'
\   }
\ }
