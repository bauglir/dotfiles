" Asynchronous Linting Engine (ALE) configuration
"
" Linting is turned off for those file types where language servers are used
" to provide the same functionality
let g:ale_fix_on_save = 1
let g:ale_fixers = {
\   '*': ['remove_trailing_lines', 'trim_whitespace'],
\ }
let g:ale_linters = {
\   'css': [],
\   'html': [],
\   'javascript': [],
\   'julia': [],
\   'scss': [],
\   'typescript': []
\ }

" Enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()

" Adjust settings for Insert mode completion compatible with ncm2. See :help
" Ncm2PopupOpen for more information
set completeopt=noinsert,menuone,noselect

" Suppress the annoying 'match x of y', 'The only match' and 'Pattern not
" found' messages
set shortmess+=c

" Write swapfiles to disk more often, this is mainly for affecting the
" CursorHold event to fire faster so it can be used for showing diagnostics.
" This also affects how fast markdown-preview.nvim rerenders previews.
set updatetime=100

" CTRL-C doesn't trigger the InsertLeave autocmd. map to <ESC> instead
inoremap <C-c> <Esc>

" When the <Enter> key is pressed while the popup menu is visible, it only
" hides the menu. This mapping closes the menu and starts a new line
inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<CR>" : "\<CR>")

" Use <Tab> to select the popup menu:
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Define normal bindings to cycle through diagnostics,
lua <<EOS
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>d', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '<space>dl', vim.diagnostic.setloclist, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
EOS

" Use Nerd Fonts to get nice icons for diagnostic messages in the sign column
call sign_define('LspDiagnosticsSignError', {'text' : '✘', 'texthl' : 'LspDiagnosticsVirtualTextError'})
call sign_define('LspDiagnosticsSignWarning', {'text' : '', 'texthl' : 'LspDiagnosticsVirtualTextWarning'})
call sign_define('LspDiagnosticsSignInformation', {'text' : '', 'texthl' : 'LspDiagnosticsVirtualTextInformation'})
call sign_define('LspDiagnosticsSignHint', {'text' : '', 'texthl' : 'LspDiagnosticsVirtualTextHint'})

" Register language servers managed through `mason.nvim`,
" `mason-lspconfig.nvim` and `nvim-lsp-installer` with ncm2 and hook up to
" native LSP functionality
lua << EOF
require('mason').setup()

local mason_lspconfig = require('mason-lspconfig')
mason_lspconfig.setup({
  ensure_installed = {
    'angularls',
    'ansiblels',
    'cssls',
    'dockerls',
    'emmet_ls',
    'grammarly',
    'graphql',
    'html',
    'jsonls',
    'julials',
    'tsserver',
    'vimls',
    'yamlls'
  }
})

local function on_attach(client, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', '<M-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', '<space>f', function () vim.lsp.buf.format {async = true} end, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', 'g0', vim.lsp.buf.document_symbol, bufopts)
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gH', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gW', vim.lsp.buf.workspace_symbol, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
end

local lspconfig = require("lspconfig")
local ncm2 = require('ncm2')
lspconfig.util.default_config = vim.tbl_extend(
  'force',
  lspconfig.util.default_config,
  {
    on_attach = on_attach,
    on_init = ncm2.register_lsp_source
  }
)

for _, server in ipairs(mason_lspconfig.get_installed_servers()) do
  lspconfig[server].setup {}
end
EOF
