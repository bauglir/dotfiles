# Dotfiles

These are my dotfiles, managed through Ansible. Inspiration taken from [this
blog post by Kyle
Espinola](https://medium.com/espinola-designs/manage-your-dotfiles-with-ansible-6dbedd5532bb)
and the corresponding [repository on
GitHub](https://github.com/kespinola/dotfiles).

To set up a new machine, ensure `pip` is available and run `./bin/setup`.
Afterwards, and for all future runs to update the configuration of a machine,
run `ansible-playbook --ask-become-pass </path/to/dotfiles/>dotfiles.yml`.

## Why Ansible?

I use different operating systems, have mixed configuration for personal and
work machines, would like to also manage available software, etc. Using a
simple symlink-based solution has worked fine for me in the past, but also
involved a lot of `git stash` and `git stash pop`, which isn't the most
convenient solution. Hence using a configuration management tool for the job.
